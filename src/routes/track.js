const express = require('express');
const fsWriteService = require('../services/fs-track-service');
const router = express.Router();

module.exports = (redisService) => {

    router.post('/', function (req, res) {
        fsWriteService.enqueueWrite(process.env.TRACKING_FILE, JSON.stringify(req.body))
            .then(() => redisService.increaseCount(req.body))
            .then(() => res.sendStatus(200))
            .catch(error => res.status(500).send(error.message));
    });

    return router;
};
