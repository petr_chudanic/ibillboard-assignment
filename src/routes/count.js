const express = require('express');
const router = express.Router();

module.exports = (redisService) => {

    router.get('/', function (req, res) {
        redisService.getCount(req.body)
            .then((count) => res.status(200).send({actualCount: count}))
            .catch(error => res.status(500).send(error.message));
    });

    return router;
};
