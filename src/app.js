const express = require('express');
const logger = require('morgan');
const redisClient = require('./redis-client');
const redisServiceProvider = require('./services/redis-track-service');

const trackRouter = require('./routes/track');
const countRouter = require('./routes/count');

const redisService = redisServiceProvider(redisClient);
const app = express();

app.use(logger('dev'));
app.use(express.json());

app.use('/track', trackRouter(redisService));
app.use('/count', countRouter(redisService));

module.exports = app;
