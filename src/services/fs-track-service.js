const fs = require('fs');
const queues = {};

class Queue {
    constructor() {
        this.queue = [];
    }

    next() {
        if (this.queue.length === 0)
            return;

        const entry = this.queue[0];
        fs.appendFile(entry.path, entry.content, 'utf8', (err) => {
            this.queue.shift();
            entry.callback(err);
        });
    }

    add(path, content, callback) {
        this.queue.push({path: path, content: content, callback: callback});
        if (this.queue.length === 1) {
            this.next();
        }
    }
}

module.exports = {
    enqueueWrite: (path, content) => {
        let queue = queues[path];
        if (queue == null)
            queue = queues[path] = new Queue;

        return new Promise((resolve, reject) => {
            queue.add(path, content, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
                queue.next();
            });
        }, error => {
            console.error(error);
            throw new Error("Error occurred while writing to file.");
        });
    }
};

