module.exports = (redisClient) => {
    return {
        increaseCount: (content) => {
            return new Promise((resolve, reject) => {
                if (typeof content.count == 'number') {
                    redisClient.get("count", (error, response) => {
                        if (error) {
                            reject(error);
                        } else {
                            const newCount = response ? Number(response) + content.count : content.count;
                            redisClient.set("count", newCount, (error) => {
                                if (error) {
                                    reject(error);
                                } else {
                                    resolve();
                                }
                            });
                        }
                    });
                } else resolve()
            }).then(() => {
            }, error => {
                console.error(error);
                throw new Error("Unable to store new information.");
            });
        },

        getCount: () => {
            return new Promise((resolve, reject) => {
                redisClient.get("count", (error, response) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(response);
                    }
                });
            }).then((count) => count ? count : 0, error => {
                console.error(error);
                throw new Error("Unable to retrieve count information.");
            });
        }
    };
};

