const redis = require('redis');

module.exports = redis.createClient({
    prefix: process.env.REDIS_KEY_PREFIX,
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
});