const request = require('supertest');
const app = require('../src/app');
const fs = require('fs');
const client = require('../src/redis-client');

beforeEach(async () => {
    if (fs.existsSync(process.env.TRACKING_FILE)) {
        const fd = fs.openSync(process.env.TRACKING_FILE, 'r+');
        fs.ftruncateSync(fd);
    }
    await new Promise((resolve, reject) => client.set("count", 0, (error) => error ? reject(error) : resolve()));
});

test('Track request without count', async () => {
    await request(app).post('/track').send({
        "some-key": "some-value"
    }).expect(200);
    const count = await new Promise((resolve, reject) => {
        client.get("count", (error, response) => error ? reject(error) : resolve(response))
    });
    expect(Number(count)).toBe(0);

    expect(fs.readFileSync(process.env.TRACKING_FILE, 'utf8')).toBe("{\"some-key\":\"some-value\"}")
});

test('Track request with count', async () => {
    await request(app).post('/track').send({
        "some-key": "some-value",
        "count": 10
    }).expect(200);
    const count = await new Promise((resolve, reject) => {
        client.get("count", (error, response) => error ? reject(error) : resolve(response))
    });
    expect(Number(count)).toBe(10);

    expect(fs.readFileSync(process.env.TRACKING_FILE, 'utf8')).toBe("{\"some-key\":\"some-value\",\"count\":10}")
});

test('Get count that is not initialized', async () => {
    const result = await request(app).get('/count').expect(200);
    expect(Number(result.body.actualCount)).toBe(0);
});

test('Get count that is initialized to 5', async () => {

    await new Promise((resolve, reject) => {
        client.set("count", 5, (error) => error ? reject() : resolve());
    });
    const result = await request(app).get('/count').expect(200);
    expect(Number(result.body.actualCount)).toBe(5);
});