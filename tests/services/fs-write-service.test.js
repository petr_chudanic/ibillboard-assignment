const fsWriteService = require('../../src/services/fs-track-service');
const mockFs = require('mock-fs');

beforeEach(() => {
    mockFs();
});

afterEach(() => {
    mockFs.restore();
});

test('Write and check file content', async () => {
    const fileContent = "new content";
    await fsWriteService.enqueueWrite("tracking-file.txt", fileContent);

    const fs = require('fs');
    expect(fs.readFileSync("tracking-file.txt", "utf8")).toBe(fileContent);
});

test('Write and check file content twice', async () => {
    const fileContent1 = "new content 1";
    const fileContent2 = "new content 2";
    await fsWriteService.enqueueWrite("tracking-file.txt", fileContent1);
    await fsWriteService.enqueueWrite("tracking-file.txt", fileContent2);

    const fs = require('fs');
    expect(fs.readFileSync("tracking-file.txt", "utf8")).toBe(fileContent1 + fileContent2);
});