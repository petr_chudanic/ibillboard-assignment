const redisServiceProvider = require('../../src/services/redis-track-service');

const mockRedisClient = (isGetSuccess, originalValue, isSetSuccess) => {
    return {
        countValue: originalValue,

        get: function(key, callback) {
            isGetSuccess ? callback(null, this.countValue) : callback(new Error(), null);
        },

        set: function(key, value, callback) {
            if (isSetSuccess) {
                this.countValue = value;
                callback(null);
            } else {
                callback(new Error());
            }
        }
    };
};

test('Increase count - from null to 10', async () => {
    const redisClientMock = mockRedisClient(true, null, true);
    const redisService = redisServiceProvider(redisClientMock);
    await redisService.increaseCount({count: 10});
    expect(redisClientMock.countValue).toBe(10);
});

test('Increase count - from 5 to 10', async () => {
    const redisClientMock = mockRedisClient(true, 5, true);
    const redisService = redisServiceProvider(redisClientMock);
    await redisService.increaseCount({count: 5});
    expect(redisClientMock.countValue).toBe(10);
});

test('Increase count - no new count', async () => {
    const redisClientMock = mockRedisClient(true, 5, true);
    const redisService = redisServiceProvider(redisClientMock);
    await redisService.increaseCount({somethingElse: "irrelevant"});
    expect(redisClientMock.countValue).toBe(5);
});

test('Increase count - error on reading from redis', async () => {
    const redisClientMock = mockRedisClient(false, 5, false);
    const redisService = redisServiceProvider(redisClientMock);
    await expect(redisService.increaseCount({count: 10})).rejects.toThrow(new Error("Unable to store new information."));
});

test('Increase count - error on writing to redis', async () => {
    const redisClientMock = mockRedisClient(true, 5, false);
    const redisService = redisServiceProvider(redisClientMock);
    await expect(redisService.increaseCount({count: 10})).rejects.toThrow(new Error("Unable to store new information."));
});

test('Retrieve count - error on reading from redis', async () => {
    const redisClientMock = mockRedisClient(false, 5, true);
    const redisService = redisServiceProvider(redisClientMock);
    await expect(redisService.getCount()).rejects.toThrow(new Error("Unable to retrieve count information."));
});

test('Retrieve count - redis contains count = 5', async () => {
    const redisClientMock = mockRedisClient(true, 5, true);
    const redisService = redisServiceProvider(redisClientMock);
    const storedCount = await redisService.getCount();
    await expect(storedCount).toBe(5);
});

test('Retrieve count - redis does not contain count', async () => {
    const redisClientMock = mockRedisClient(true, null, true);
    const redisService = redisServiceProvider(redisClientMock);
    const storedCount = await redisService.getCount();
    await expect(storedCount).toBe(0);
});