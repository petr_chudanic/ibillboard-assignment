## Description
This is node.js application that provides following functionality.

1. receives HTTP POST requests only on a "/track" route
    - gets data in JSON format passed in the request body
    - saves the JSON data into a local file (append)
    - if the data contains a "count" parameter, the application increments the value of the "count" key by the value of the 'count' parameter in a Redis database
2. receives HTTP GET requests only on a "/count" route
    - returns the value of the "count" key from the Redis database

### Requirements
- Node.js version 10
- installed Redis database
- Environment variables - application uses environment variable defined below to work properly
    - **TRACKING_FILE** path to file for tracking
    - **PORT port where** application is running
    - **REDIS_KEY_PREFIX**  redis key prefix
    - **REDIS_HOST** host of Redis database
    - **REDIS_PORT** port of Redis database

### Installation
- run following command to download and install dependencies
```
npm install
``` 

### Run dev instance
Start development instance using
```
npm run dev
```
This will spin up application using nodemon for development purposes. Environmental variables are used from 
./config/dev.env file. 

### Run tests
```
nmp test
```
This will execute all tests. Environmental variables for testing are loaded from ./config/test.env file.

### Deployment
To deploy you can build docker image using provided Dockerfile. Already built one is available as
chudanic/ibilbo-node:latest.

To simplify preparation of environment - setting of environment variables and Redis DB - there is also 
docker-compose.yml available that creates Redis DB in on container, start application in other one making it accessible
on port 3000.
  